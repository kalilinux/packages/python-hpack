Metadata-Version: 1.1
Name: hpack
Version: 1.1.0
Summary: Pure-Python HPACK header compression
Home-page: http://hyper.rtfd.org
Author: Cory Benfield
Author-email: cory@lukasa.co.uk
License: MIT License
Description: ========================================
        hpack: HTTP/2 Header Encoding for Python
        ========================================
        
        .. image:: https://raw.github.com/Lukasa/hyper/development/docs/source/images/hyper.png
        
        .. image:: https://travis-ci.org/Lukasa/hpack.png?branch=master
            :target: https://travis-ci.org/Lukasa/hpack
        
        This module contains a pure-Python HTTP/2 header encoding (HPACK) logic for use
        in Python programs that implement HTTP/2. It also contains a compatibility
        layer that automatically enables the use of ``nghttp2`` if it's available.
        
        Contributing
        ============
        
        ``hpack`` welcomes contributions from anyone! Unlike many other projects we are
        happy to accept cosmetic contributions and small contributions, in addition to
        large feature requests and changes.
        
        Before you contribute (either by opening an issue or filing a pull request),
        please `read the contribution guidelines`_.
        
        .. _read the contribution guidelines: http://hyper.readthedocs.org/en/development/contributing.html
        
        License
        =======
        
        ``hpack`` is made available under the MIT License. For more details, see the
        ``LICENSE`` file in the repository.
        
        Authors
        =======
        
        ``hpack`` is maintained by Cory Benfield, with contributions from others. For
        more details about the contributors, please see ``CONTRIBUTORS.rst``.
        
        
        Release History
        ===============
        
        1.1.0 (2015-07-07)
        ------------------
        
        - Add support for emitting 'never indexed' header fields, by using an optional
          third element in the header tuple. With thanks to @jimcarreer!
        
        1.0.1 (2015-04-19)
        ------------------
        
        - Header fields that have names matching header table entries are now added to
          the header table. This improves compression efficiency at the cost of
          slightly more table operations. With thanks to `Tatsuhiro Tsujikawa`_.
        
        .. _Tatsuhiro Tsujikawa: https://github.com/tatsuhiro-t
        
        1.0.0 (2015-04-13)
        ------------------
        
        - Initial fork of the code from `hyper`_.
        
        .. _hyper: https://hyper.readthedocs.org/
        
Platform: UNKNOWN
Classifier: Development Status :: 3 - Alpha
Classifier: Intended Audience :: Developers
Classifier: License :: OSI Approved :: MIT License
Classifier: Programming Language :: Python
Classifier: Programming Language :: Python :: 2
Classifier: Programming Language :: Python :: 2.7
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.4
Classifier: Programming Language :: Python :: Implementation :: CPython
